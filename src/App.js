import './App.css';
import Navbar from './components/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from "./components/pages/Home"
import Service from './components/pages/Service';
import SingUp from './components/pages/SingUp';
import category from './components/pages/category';
import appointment from './components/pages/Appointment';

function App() {
  return ( 
    <>
      <Router>
        <Navbar/>
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/services" component={Service}></Route>
          <Route path="/signup" component={SingUp}></Route>
          <Route path="/category" component={category}></Route>
          <Route path="/appointment" component={appointment}></Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
