import React, { useState, useEffect } from 'react'
import CardItem from './CardItem'
import './Cards.css'
import SimpleBackdrop from './Backdrop.js'

function Cards(props) {
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        fetch('https://agendabackend.azurewebsites.net/Api/Category', {
            method: 'GET',
            headers: {
                'Authorization': '1xp12z092834uwioF*324jisdfAppafCAp234EEE32szzzLLq2323IIsdsdfsffZ>',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then((json) => {
                console.log(json);
                json.data.filter(x => !x.deleted).forEach(element => {
                    console.log(element);
                    let category = {
                        categoryId: element.categoryId,
                        name: element.name,
                        description: element.description,
                        image: element.image
                    }
                    setCategories((categories) => [...categories, category]);
                });
            })
            .catch(error => console.error(error));
    }, []);

    return (
        <div className={`cards ${props.className}`}>
            <h1>Categorías de servicios</h1>
            <div className="cards__container">
                <div className="cards__wrapper">
                    <ul className="cards__items">
                        {categories.length == 0 ? (
                             <SimpleBackdrop></SimpleBackdrop>
                        ) :
                            (
                                categories.map(category =>
                                    <CardItem
                                        key={category.categoryId}
                                        src={category.image}
                                        text={category.description}
                                        label={category.name}
                                        path={`/services?categoryId=${category.categoryId}`}
                                        categoryId={category.categoryId}
                                    />

                                )
                            )
                        }

                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards
