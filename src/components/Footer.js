import React from 'react'
import Button from './Button'
import './Footer.css'
import { Link } from 'react-router-dom'

function Footer() {
    return (
        <div className="footer-container">
            <section className="footer-subscription">
                <p className="footer-subscription-heading">
                    Capelli Salón, siempre con los mejores servicios. Te invitamos a revisar nuestros servicios.
                </p>
                <p className="footer-subscription-text">
                    Podras realizar una cita desde acá y darle seguimiento a la misma. Si deseas un contacto directo, podrás realizarlo acá mismo.
                </p>
                {/* <div className="input-areas">
                    <form>
                        <input
                            type="email"
                            name="email"
                            placeholder="Email"
                            className="footer-input"
                        />
                        <Button buttonStyle="btn--outline">Suscribirse</Button>

                    </form>
                </div> */}
            </section>
            <div className="footer-links">
                <div className="footer-link-wrapper">
                    <div className="footer-link-items">
                        <h2>Acerca de nosotros</h2>
                        <Link to="/signup">Hot it works</Link>
                        <Link to="/">Testimonios</Link>
                        <Link to="/">Nuestro equipo</Link>
                        <Link to="/">Acerca de</Link>
                        <Link to="/">Términos y condiciones</Link>
                    </div>
                    <div className="footer-link-items">
                        <h2>Acerca de nosotros</h2>
                        <Link to="/signup">Hot it works</Link>
                        <Link to="/">Testimonios</Link>
                        <Link to="/">Nuestro equipo</Link>
                        <Link to="/">Acerca de</Link>
                        <Link to="/">Términos y condiciones</Link>
                    </div>
                </div>
                <div className="footer-link-wrapper">
                    <div className="footer-link-items">
                        <h2>Acerca de nosotros</h2>
                        <Link to="/signup">Hot it works</Link>
                        <Link to="/">Testimonios</Link>
                        <Link to="/">Nuestro equipo</Link>
                        <Link to="/">Acerca de</Link>
                        <Link to="/">Términos y condiciones</Link>
                    </div>
                    <div className="footer-link-items">
                        <h2>Acerca de nosotros</h2>
                        <Link to="/signup">Hot it works</Link>
                        <Link to="/">Testimonios</Link>
                        <Link to="/">Nuestro equipo</Link>
                        <Link to="/">Acerca de</Link>
                        <Link to="/">Términos y condiciones</Link>
                    </div>
                </div>
                <div className="footer-link-wrapper">
                    <div className="footer-link-items">
                        <h2>Acerca de nosotros</h2>
                        <Link to="/signup">Hot it works</Link>
                        <Link to="/">Testimonios</Link>
                        <Link to="/">Nuestro equipo</Link>
                        <Link to="/">Acerca de</Link>
                        <Link to="/">Términos y condiciones</Link>
                    </div>
                    <div className="footer-link-items">
                        <h2>Acerca de nosotros</h2>
                        <Link to="/signup">Hot it works</Link>
                        <Link to="/">Testimonios</Link>
                        <Link to="/">Nuestro equipo</Link>
                        <Link to="/">Acerca de</Link>
                        <Link to="/">Términos y condiciones</Link>
                    </div>
                </div>
            </div>
            <section className="social-media">
                <div className="social-media-wrap">
                    <div className="footer-logo">
                        <Link className="social-logo">
                            Capelli Salón<i className="fab fa-typo3"></i>
                        </Link>
                    </div>
                    <small className="website-rights">Capelli Salón @{new Date().getFullYear()}</small>
                    <div className="social-icons">
                        <Link className="social-icon-link facebook"
                            to="/https://www.facebook.com/Capelli-Sal%C3%B3n-Wellness-Spa-107515804082340"
                            target="_blank"
                            aria-label="Facebook">
                            <i className="fab fa-facebook-f"></i>
                        </Link>
                        <Link className="social-icon-link instagram"
                            to="/https://www.instagram.com/explore/locations/538171826647129/capelli-salon-y-spa/?hl=es"
                            target="_blank"
                            aria-label="Facebook">
                            <i className="fab fa-instagram"></i>
                        </Link>
                        <Link className="social-icon-link youtube"
                            to="/https://www.facebook.com/Capelli-Sal%C3%B3n-Wellness-Spa-107515804082340"
                            target="_blank"
                            aria-label="Youtube">
                            <i className="fab fa-youtube"></i>
                        </Link>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Footer
