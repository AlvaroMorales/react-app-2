import React, { useState, useEffect } from 'react';
import Button from "./Button"
import { Link } from 'react-router-dom';
import './Navbar.css'
function Navbar() {
    const [click, setClick] = useState(false);
    const [autenticated, setAutenticated] = useState(true);
    const [button, setButton] = useState(true);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showButton = () => {
        if (window.innerWidth <= 960) {
         setButton(false);   
        } else{
            setButton(true);
        }
    };
window.addEventListener('resize', showButton);
useEffect(() => {
    showButton()
}, []);
return (
        <>
            <nav className="navbar">
                <div className="navbar-container">
                    <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
                        Capelli Salón <i className="fab fa-cut" />
                    </Link>
                    <div className="menu-icon" onClick={handleClick}>
                        <i className={click ? "fas fa-times" : "fas fa-bars"} />
                    </div>
                    <ul className={click ? "nav-menu active" : "nav-menu"}>
                        <li className="nav-item">
                            <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/category" className="nav-links" onClick={closeMobileMenu}>
                                Categorías
                            </Link>
                        </li> 
                        <li className="nav-item">
                            <Link to="/about" className="nav-links" onClick={closeMobileMenu}>
                                Acerca de
                            </Link>
                        </li>
                        {
                        autenticated ? (
                            <li className="nav-item">
                                <Link to="/Appointment" className="nav-links" onClick={closeMobileMenu}>
                                    Mis citas
                                </Link>
                            </li>)
                             : ""
                            // (<li className="nav-item">
                            //     <Link to="/signup" className="nav-links" onClick={closeMobileMenu}>
                            //         Login
                            //     </Link>
                            // </li>)
                        }
                    </ul>
                    {
                        autenticated ? (button && <Button buttonStyle="btn--outline">Cerrar sesión</Button>)
                        : (button && <Button buttonStyle="btn--outline">Login</Button>)
                    }
                </div>
            </nav>
        </>
    )
}

export default Navbar
