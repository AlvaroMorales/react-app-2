import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


const useStyles = makeStyles((theme) => ({
    button: {
        display: 'block',
        marginTop: theme.spacing(2),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        backgroundColor: "transparent",
        color: "#fff !important",
        "& .MuiFormLabel-root": { color: "white" },
        "& .MuiInputBase-root": { color: "white" },
        "& .MuiInput-underline:before": { color: "white" },
        "& .MuiInput-underline:hover:not(.Mui-disabled):before": { color: "white" },
    },
    background: {
        backgroundColor: "#b3a58b82",
        color: "#fff !important",
    },
}));
export default function SelectService(props) {
  console.log(props);
    const classes = useStyles();
  
    return (
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="grouped-select">Grouping</InputLabel>
          <Select defaultValue="" id="grouped-select">
            <ListSubheader>Category 1</ListSubheader>
            <MenuItem value={1}>Option 1</MenuItem>
            <MenuItem value={2}>Option 2</MenuItem>
            <ListSubheader>Category 2</ListSubheader>
            <MenuItem value={3}>Option 3</MenuItem>
            <MenuItem value={4}>Option 4</MenuItem>
          </Select>
        </FormControl>
      </div>
    );
  }