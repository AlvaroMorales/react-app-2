import * as React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        "& .MuiTableContainer-root": {
            color: "white !important",
        },
        "& .MuiTableCell-head": {
            color: "white !important",
        },
        "& .MuiTableCell-body": {
            color: "white !important",
        }
        ,
        "& caption": {
            color: "white !important",
        }

    },
    background: {
        backgroundColor: "#b3a58b82",
        color: "#fff !important",
    },
    padding: {
        paddingRight: '30px !important',
        paddingLeft: '30px !important',
        paddingTop: '30px !important',
    },
    stepperTextColor: {
        color: "#fff !important",
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    actionsContainer: {
        marginBottom: theme.spacing(2),
    },
    resetContainer: {
        padding: theme.spacing(3),
        backgroundColor: "#b3a58b82",
        color: "#fff !important",
    },
    white: {
        color: "#fff !important"
    }
}));

const rows = [
    createData('Inicia', 159, 6.0, 24, 4.0),
    createData('Servicio', 237, 9.0, 37, 4.3),
    createData('Técnico', 262, 16.0, 24, 6.0),
];

export default function TableAppointment(props) {
    const classes = useStyles();
    return (
        <TableContainer component={Paper} className={classes.background} >
            <Table className={classes.root} sx={{ minWidth: 650 }} aria-label="caption table">
                <caption>Detalle de servicios a brindar</caption>
                <TableHead>
                    <TableRow>
                        <TableCell>No</TableCell>
                        <TableCell align="right">Servicio</TableCell>
                        <TableCell align="right">Técnico</TableCell>
                        <TableCell align="right">Inicia</TableCell>
                        <TableCell align="right">Termina</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell align="right">{row.calories}</TableCell>
                            <TableCell align="right">{row.fat}</TableCell>
                            <TableCell align="right">{row.carbs}</TableCell>
                            <TableCell align="right">{row.protein}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}