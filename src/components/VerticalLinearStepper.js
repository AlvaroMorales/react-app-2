import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TableAppointment from './TableAppointment'
import DateComponent from './DateComponent'
import SelectEmployee from './SelectEmployee'
import SelectService from './SelectService'
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    "& .MuiStepIcon-completed": { color: "white" },
    "& .MuiStepLabel-label": { color: "white" }
  },
  background: {
    backgroundColor: "#b3a58b82",
    color: "#fff !important",
  },
  padding: {
    paddingRight: '30px !important',
    paddingLeft: '30px !important',
    paddingTop: '30px !important',
  },
  stepperTextColor: {
    color: "#fff !important",
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
    backgroundColor: "#b3a58b82",
    color: "#fff !important",
  },
  white: {
    color: "#fff !important"
  },
}));
function getSteps() {
  return ['Fecha y hora', 'Servicio', 'Empleado'];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return `Selecciona la fecha y hora de su cita a agendar.`;
    case 1:
      return 'Seleccione el servicio a agendar.';
    case 2:
      return `Seleccione el empleado de su preferencia.`;
    default:
      return 'Unknown step';
  }
}
function getStepComponent(step, categories) {
  switch (step) {
    case 0:
      return <Grid container spacing={2}>
        <Grid item sm={8} xs={12} >
          <DateComponent></DateComponent>
        </Grid>
      </Grid>
    case 1:
      return <Grid container spacing={2}>
        <Grid item sm={8} xs={12} >
          <SelectService props={categories}></SelectService>
        </Grid>
      </Grid>
    case 2:
      return <Grid container spacing={2}>
        <Grid item sm={8} xs={12} >
          <SelectEmployee></SelectEmployee>
        </Grid>
      </Grid>;
    default:
      return 'Unknown step';
  }
}



export default function VerticalLinearStepper() {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    fetch('https://agendabackend.azurewebsites.net/Api/Category', {
      method: 'GET',
      headers: {
        'Authorization': '1xp12z092834uwioF*324jisdfAppafCAp234EEE32szzzLLq2323IIsdsdfsffZ>',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
      .then(response => response.json())
      .then((json) => {
        console.log(json);
        json.data.filter(x => !x.deleted).forEach(element => {
          console.log(element);
          let category = {
            categoryId: element.categoryId,
            name: element.name,
            description: element.description,
            image: element.image,
            services: element.service
          }
          setCategories((categories) => [...categories, category]);
        });
      })
      .catch(error => console.error(error));
  }, []);
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <Grid container spacing={2} className={classes.padding}>
      <Grid item sm={8} xs={12} >
        <div className={classes.root} >
          <Stepper activeStep={activeStep} className={classes.background} orientation="vertical">
            {steps.map((label, index) => (
              <Step key={label} className={classes.stepperTextColor}>
                <StepLabel>{label}</StepLabel>
                <StepContent>
                  <Typography className={classes.white}>{getStepContent(index)}</Typography>
                  <div className={classes.actionsContainer}>
                    <div>
                      {getStepComponent(index, categories)}
                      <Grid container spacing={2} className={classes.padding}>

                        <Grid item sm={4} xs={12}>
                          <Button
                            disabled={activeStep === 0}
                            onClick={handleBack}
                            className={classes.button + " " + classes.white}
                          >
                            Regresar
                          </Button>
                          <Button
                            variant="contained"
                            color="primary"
                            onClick={handleNext}
                            className={classes.button + " " + classes.white}
                          >
                            {activeStep === steps.length - 1 ? 'Agregar servicio' : 'Continuar'}
                          </Button>
                        </Grid>
                      </Grid>
                    </div>
                  </div>
                </StepContent>
              </Step>
            ))}
          </Stepper>
          {activeStep === steps.length && (
            <Paper square elevation={0} className={classes.resetContainer}>
              <Typography className={classes.white}>All steps completed - you&apos;re finished</Typography>
              <Button onClick={handleReset} className={classes.button + " " + classes.white}>
                Agregar un nuevo servicio
              </Button>
            </Paper>
          )}
        </div>
      </Grid>
      <Grid item sm={4} xs={12}>
        <TableAppointment className={classes.background}></TableAppointment>
        <Grid container justify="flex-end" item sm={12} xs={12} className={classes.button}>
          <Button variant="contained" className={classes.button}>Secondary</Button>
          <Button variant="contained" color="primary" className={classes.button}>Agendar</Button>
        </Grid>
      </Grid>
    </Grid>
  );
}