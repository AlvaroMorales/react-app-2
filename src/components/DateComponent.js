import 'date-fns';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
const useStyles = makeStyles((theme) => ({
  formControl: {
    color: "#fff !important",
    "& .MuiInputBase-root": { color: "white", borderBottom: " 1px solid #fff;" },
    "& .MuiFormLabel-root": { color: "white" },
    "& .MuiIconButton-label": { color: "white" },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": { color: "white" },
    "& .MuiPickersModal-dialog:first-child": {
      backgroundColor: "#b3a58b82",
      color: "#fff !important",
    },
  },
}));
export default function DateComponent() {
  const classes = useStyles();
  // The first commit of Material-UI
  const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };


  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justifyContent="space-around">
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Fecha de inicio"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          className={classes.formControl}
        />
        <KeyboardTimePicker
          margin="normal"
          id="time-picker"
          label="Hora de inicio"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
          className={classes.formControl}
        />
      </Grid>
    </MuiPickersUtilsProvider>
  );
}