import React, { useState, useEffect } from 'react'
import CardItemMUI from './CardItemMUI';
import './Cards.css'
import SimpleBackdrop from './Backdrop'

function Cards(props) {
    const [services, setServices] = useState([]);
let params= new URLSearchParams(window.location.search);
let categoryId= params.get("categoryId");
    useEffect(() => {
        fetch(`https://agendabackend.azurewebsites.net/Api/Service/ServicesByCategory/${categoryId}`, {
            method: 'GET',
            headers: {
                'Authorization': '1xp12z092834uwioF*324jisdfAppafCAp234EEE32szzzLLq2323IIsdsdfsffZ>',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then((json) => {
                console.log(json);
                json.data.filter(x => !x.deleted).forEach(element => {
                    console.log(element);
                    let service = {
                        serviceId: element.serviceId,
                        name: element.name,
                        description: element.description,
                        image: element.image
                    }
                    setServices((services) => [...services, service]);
                });
            })
            .catch(error => console.error(error));
    }, []);

    return (
        <div className={`cards ${props.className}`}>
            <h4>Servicios por categoría</h4>
            <div className="cards__container">
                <div className="cards__wrapper">
                    <ul className="cards__items">
                        {services.length == 0 ? (
                             <SimpleBackdrop></SimpleBackdrop>
                             ) :
                            (
                                services.map(service =>
                                    <CardItemMUI
                                        key={service.serviceId}
                                        src={service.image}
                                        text={service.description}
                                        label={service.name}
                                        path={`/pages/Create`}
                                    />

                                ) 
                            )
                        }
 
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards
