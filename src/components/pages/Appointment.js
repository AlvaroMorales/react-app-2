import { React, setState, useState } from 'react'
import VerticalLinearStepper from '../VerticalLinearStepper'

 export default function appointment() {
    return (
        <div className="appointment">
            <h1>Crear cita</h1>
            <VerticalLinearStepper></VerticalLinearStepper>
        </div>
    )
}
