import React from 'react';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin: 20,
    boxShadow: '0 6px 20px rgb(254 255 242 / 66%)',
    background: '#b9ad754d',
  },
  media: {
    height: 140,
  },
});
export default function CardItemMUI(props) {
  const classes = useStyles();
  const history = useHistory();
  const createAppointment = () => {
    let path = `Appointment/${props.serviceId}`;
    history.push(path);
  }
  return (

        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={props.src}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" style={{ color: "#ffffff" }} component="h2">
                {props.label}
              </Typography>
              <Typography variant="body2" style={{ color: "#ffffff" }} color="textSecondary" component="p">
                {props.text}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="large" style={{ color: "#ffffff" }} onClick={createAppointment}>
              Crear cita
            </Button>
          </CardActions>
        </Card>
  );
}
