import React from 'react'
import { Button } from "./Button"
import "./MainSection.css"
function MainSection() {
    return (
        <div className="main-container">
            <h1>Capelli Salón</h1>
            <p>Desde acá podras realizar una cita...</p>
            <div className="main-btns">
                <Button className="btns" buttonStyle="btn--outline"
                    buttonSize="btn--large">
                    Iniciar sesión
                </Button>
                <Button className="btns" buttonStyle="btn--primary"
                    buttonSize="btn--large">
                    Explorar
                </Button>
            </div>
        </div>
    )
}

export default MainSection
